require_relative 'db'

class EuropereDb < Db
  def initialize(options)
    super(options)
    @newsletter = @db[:newsletter]
  end

  def delete(addresses)
    @newsletter.where({email: addresses}).delete
  end
end
