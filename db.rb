require 'sequel'
require 'logger'

class Db
  def initialize(options)
    @db = Sequel.connect(options[:url], user: options[:user], password: options[:password])
    @db.loggers << Logger.new($stderr)
  end

end
