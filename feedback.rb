require 'mail'
require 'json'
require 'pp'

class Feedback
  attr_reader :addresses, :type

  def initialize(mail)
    @mail = mail
    @addresses = collect_addresses || []
  end

  protected

  def collect_addresses
    from = @mail.from.first
    if from == 'no-reply@sns.amazonaws.com'
      sns_mail
    elsif ['complaints@eu-west-1.email-abuse.amazonses.com'].include?(from) && @mail.multipart?  # FIXME: address may change.
      # ses_feedback
    else
      # $stderr.puts 'Unknown mail type:'
      # $stderr.puts mail.to_s
    end
  end

  # Feedback SES email.
  def ses_feedback
    unless @mail.multipart?
      puts 'Not a multipart email.'
      return
    end
    forwarded_mail = Mail.read_from_string(@mail.parts.last.body)
    forwarded_mail.to
  end

  # SNS notifications.
  def sns_mail
    json = JSON.parse @mail.body.to_s
    return unless json['Type'] == 'Notification'

    message = JSON.parse json['Message']
    @type = message['notificationType']

    recipients =
      if @type == 'Bounce'
        sns_bounce_recipients message
      elsif @type == 'Complaint'
        message['complaint']['complainedRecipients']
      else
        $stderr.puts "Unknown SNS message type=#{@type}."
      end
    recipients.map { |e| e['emailAddress'] } if recipients
  end

  # https://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-contents.html#bounce-types
  def sns_bounce_recipients(message)
    bounce = message['bounce']
    if bounce['bounceType'] == 'Permanent'
      bounce['bouncedRecipients']
    else
      []
    end
  end

end
