require_relative 'config'
require_relative 'feedback'
require_relative 'europere_db'
require_relative 'retailp_db'

require 'mail'

Mail.defaults do
  retriever_method :imap, Config[:mail]
end

DbModel = Object.const_get(Config[:db_model] + 'Db')
db = DbModel.new(Config[:db])

# Mark as SEEN.
# https://github.com/mikel/mail/issues/422

# Do not delete messages, but mark as SEEN.
Mail.find(:what => :last, :keys => ['NOT', 'SEEN'], :count => 1000, :order => :desc) do |mail, imap, message_id|
  feedback = Feedback.new mail
  addresses = feedback.addresses
  if addresses.size > 0
    # puts "Deleting addresses (type: #{feedback.type}): "
    puts addresses
    db.delete addresses
  end
  imap.uid_store message_id, '+FLAGS', [:Seen]
end
