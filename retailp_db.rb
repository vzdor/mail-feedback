require_relative 'db'

class RetailpDb < Db
  def initialize(options)
    super(options)
    @users = @db[:users]
    @profiles = @db[:notification_profiles]
  end

  # See app/models/newsletter_delivery.rb in retailp.
  def delete(addresses)
    users = @users.where({email: addresses}).all
    users.each do |user|
      @profiles.where({user_id: user[:id]}).update({newsletter_receive: false})
    end
  end
end
