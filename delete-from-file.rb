require_relative 'config'
require_relative 'feedback'
require_relative 'europere_db'

require 'mail'

Mail.defaults do
  retriever_method :imap, Config[:mail]
end

DbModel = Object.const_get(Config[:db_model] + 'Db')
db = DbModel.new(Config[:db])

File.read('addrs').each_line do |address|
  puts address
  db.delete address.chomp
end
