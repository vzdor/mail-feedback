require 'minitest/autorun'
require_relative '../retailp_db'

require 'sequel'

DB = Sequel.connect('postgres:///retailp_feedback_test')

DB.drop_table 'users' rescue nil
DB.drop_table 'notification_profiles' rescue nil

DB.create_table 'users' do
  Integer :id
  String :email
end

DB.create_table 'notification_profiles' do
  Integer :user_id
  Boolean :newsletter_receive
end

Users = DB[:users]
Profiles = DB[:notification_profiles]

class RetailpDbTest < MiniTest::Unit::TestCase
  def setup
    Users.insert({email: 'bob1@com', id: 1})
    Users.insert({email: 'bob2@com', id: 2})
    Users.insert({email: 'bob3@com', id: 2})
    Profiles.insert({user_id: 1, newsletter_receive: true})
    Profiles.insert({user_id: 2, newsletter_receive: true})
    Profiles.insert({user_id: 3, newsletter_receive: true})
  end

  def test_updates_newsletter_receive_field
    db = RetailpDb.new(url: 'postgres:///retailp_feedback_test')
    db.delete(['bob1@com', 'bob2@com'])

    profile_flags = Profiles.all
                    .sort { |p1, p2| p1[:user_id] <=> p2[:user_id] }
                    .map { |p| p[:newsletter_receive] }

    assert_equal [false, false, true], profile_flags
  end
end
