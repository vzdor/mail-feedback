require 'minitest/autorun'
require_relative '../feedback'

require 'mail'

class FeedbackTest < MiniTest::Unit::TestCase
  def test_sns_complaint
    mail = Mail.read './test/fixtures/complaint-sns.txt'
    feedback = Feedback.new mail
    assert_equal feedback.addresses, ["complaint@simulator.amazonses.com"]
  end

  def test_sns_bounce
    mail = Mail.read './test/fixtures/bounce-sns.txt'
    feedback = Feedback.new mail
    assert_equal ["bounce@simulator.amazonses.com"], feedback.addresses
  end

  def test_sns_bounce_not_permanent
    mail = Mail.read './test/fixtures/bounce-sns--not-permanent.txt'
    feedback = Feedback.new mail
    assert_equal feedback.addresses, []
  end

  def test_not_sns
    mail = Mail.read './test/fixtures/complain-forward.txt'
    feedback = Feedback.new mail
    assert_equal feedback.addresses, []

  end
end
