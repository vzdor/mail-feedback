require 'minitest/autorun'
require_relative '../europere_db'

require 'sequel'

DB = Sequel.connect('mysql://localhost/europere_feedback_test')
DB.drop_table :newsletter rescue nil
DB.create_table :newsletter do
  String :email
end

class EuropereTest < MiniTest::Unit::TestCase
  def setup
    newsletter = DB[:newsletter]
    newsletter.insert({email: 'bob1@com'})
    newsletter.insert({email: 'bob2@com'})
    newsletter.insert({email: 'x@com'})
  end

  def test_deletes_addresses
    newsletter = DB[:newsletter]
    assert_equal 3, newsletter.count

    db = EuropereDb.new(url: 'mysql://localhost/europere_feedback_test')
    db.delete(['bob1@com', 'bob2@com'])

    assert_equal 1, newsletter.count
    assert_equal({email: 'x@com'}, newsletter.first)
  end
end
